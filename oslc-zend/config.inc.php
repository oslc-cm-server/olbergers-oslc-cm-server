<?php

/**
 * This file is (c) Copyright 2009 by Olivier BERGER, Institut
 * TELECOM
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * This program has been developed in the frame of the HELIOS
 * project with financial support of its funders.
 *
 */

  /* $Id$ */


// Define the backend tracker type : by defaut : mantis
defined('TRACKER_TYPE')
    || define('TRACKER_TYPE', (getenv('TRACKER_TYPE') ? getenv('TRACKER_TYPE') : 'mantis'));

switch (TRACKER_TYPE) {
	case 'mantis':
		// Initialize the Mantis environment necessary to plug to its internal API

		// this is supposed to be placed into mantis_top_level_dir/www/oslc-zend/
		defined('MANTIS_DIR')
    		|| define('MANTIS_DIR', (getenv('MANTIS_DIR') ? getenv('MANTIS_DIR') : dirname( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR));
		
		$t_mantis_dir = MANTIS_DIR;

		// TODO : explain the following line :
		$g_bypass_headers = true;
		require($t_mantis_dir.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'constant_inc.php' );
		require_once( $t_mantis_dir . 'core.php' );
		require_once( $t_mantis_dir . 'core/summary_api.php' );

		break;
	case 'demo':
		break;
	case 'fusionforge':
		define('APPLICATION_PATH', '/usr/share/gforge/www/tracker/oslc-zend/application');
		require(APPLICATION_PATH.'/../../../env.inc.php');
	    require_once $gfwww.'include/pre.php';
		break;
	default:
		throw new Exception('Unsupported TRACKER_TYPE : '. TRACKER_TYPE .' !');
		break;
}

// Define the backend tracker type : by defaut : mantis
defined('AUTH_TYPE')
    || define('AUTH_TYPE', (getenv('AUTH_TYPE') ? getenv('AUTH_TYPE') : 'basic'));

switch (AUTH_TYPE) {
	case 'basic':
		break;
	case 'oauth':
		switch (TRACKER_TYPE) {
			case 'mantis':
				// Initialize the Mantis environment necessary to plug to its internal API

				// this is supposed to be placed into mantis_top_level_dir/www/oslc-zend/
				// TODO : render this customizable in .htaccess much like TRACKER_TYPE above
				$t_mantis_dir = dirname( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR;

				// TODO : explain the following line :
				$g_bypass_headers = true;
				require_once( $t_mantis_dir . 'plugins/OauthAuthz/oauth/DbOAuthDataStore.inc.php' );

				break;
			default:
				throw new Exception('Unsupported oauth AUTH_TYPE for TRACKER_TYPE: '. TRACKER_TYPE .' !');
				break;
		}
		break;
	default:
		throw new Exception('Unsupported AUTH_TYPE : '. AUTH_TYPE .' !');
		break;
}
