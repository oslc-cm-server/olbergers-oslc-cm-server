<?php

/**
 * This file is (c) Copyright 2010 by Olivier BERGER, Institut TELECOM
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * This program has been developed in the frame of the HELIOS
 * project with financial support of its funders.
 *
 */

/**
 * This is the phpunit bootstrap script which initializes a base MantisBt environment
 * 
 *  The environment is setup so that one can use a locally checked-out OSLC-CM server copy together with a copy of Mantis code.
 *  The (not so) "unit" tests will be run in order to connect that Mantis code to a running DB. 
 */

/* $Id: CmController.php 69 2009-12-17 21:35:25Z berger_o $ */

// may need to inspire ourselves from :
// http://weierophinney.net/matthew/archives/190-Setting-up-your-Zend_Test-test-suites.html

#------
# CUSTOMIZE HERE

# Proceed to definition of constant TRACKER_TYPE for later inclusions

# CUSTOMIZE THIS if you want to set the current TRACKER_TYPE for all tests
# (can be overridden by env. variable TRACKER_TYPE)
# If mantis is chosen, see 'mantis-config_inc.php' also
$CURRENT_TRACKER_TYPE='mantis';

# CUSTOMIZE THIS if you want to set the current MANTIS_DIR for all tests
# can be overridden by env. variable MANTIS_DIR
#$CURRENT_MANTIS_DIR='/var/www/mantisbt/';
$CURRENT_MANTIS_DIR='/home/mdhar/public_html/mantisbt/';

# END OF CUSTOMIZATIONS
#------

# first, we want to see broken parts
error_reporting( E_ALL | E_STRICT );

# Env variable overrides the default settings in $CURRENT_TRACKER_TYPE above
$CURRENT_TRACKER_TYPE = getenv('TRACKER_TYPE') ? getenv('TRACKER_TYPE') : $CURRENT_TRACKER_TYPE;
//# Make sure the TRACKER_TYPE is not defined already
//if (defined('TRACKER_TYPE')) {
//	throw new Exception('Must not define TRACKER_TYPE to '. TRACKER_TYPE .' before including current file');
//}
defined('TRACKER_TYPE') || define('TRACKER_TYPE', $CURRENT_TRACKER_TYPE);
putenv('TRACKER_TYPE='.$CURRENT_TRACKER_TYPE);   		
#print 'Current TRACKER_TYPE: '.TRACKER_TYPE;

# Define APPLICATION_PATH to point to the Zend app
//if (defined('APPLICATION_PATH')) {
//	throw new Exception('Must not define APPLICATION_PATH before including current file');
//}
defined('APPLICATION_PATH') ||
	define('APPLICATION_PATH',  dirname( dirname( __FILE__ )). DIRECTORY_SEPARATOR . 'oslc-zend/application/');
#print 'Application path (APPLICATION_PATH): '.APPLICATION_PATH;

// set this so that exceptions are properly reported
define('APPLICATION_ENV','testing');

// Mantis specific part
if ($CURRENT_TRACKER_TYPE == 'mantis' ) {

	# Env variable overrides the default settings in $CURRENT_TRACKER_TYPE above
	$CURRENT_MANTIS_DIR = getenv('MANTIS_DIR') ? getenv('MANTIS_DIR') : $CURRENT_MANTIS_DIR;
	//# Make sure the TRACKER_TYPE is not defined already
	//if (defined('MANTIS_DIR')) {
	//	throw new Exception('Must not define MANTIS_DIR to '. MANTIS_DIR .' before including current file');
	//}
	defined('MANTIS_DIR') ||
		define('MANTIS_DIR', $CURRENT_MANTIS_DIR);
	#print 'Mantis dir (MANTIS_DIR): '. MANTIS_DIR;

	# Define where the specific customizations will be defined
	# This will be loaded later when the Mantis controller will load core.php
	getenv('MANTIS_CONFIG')
    		|| putenv('MANTIS_CONFIG='. dirname( __FILE__ ). DIRECTORY_SEPARATOR .'mantis-config_inc.php');
    #print 'Mantis config (MANTIS_CONFIG env var): '. getenv('MANTIS_CONFIG');

    // Attempt at restoring the expected Mantis environment so phpunit can work (looks similar to what is found in Mantis core.php)
	require_once( MANTIS_DIR.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'constant_inc.php' );
	// we don't want zlib compression
	define('COMPRESSION_DISABLED','1');
	// Include default configuration settings
	include( MANTIS_DIR.'config_defaults_inc.php' );
    		

	// Finally we definie this so that in mantis.inc.php in the run controller, the globals can be initialized properly 
	defined('MANTIS_GLOBALS_SETAGAIN') ||
		define('MANTIS_GLOBALS_SETAGAIN', '1');

}

