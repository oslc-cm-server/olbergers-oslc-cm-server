<?php
require_once 'PHPUnit/Framework.php';


define(APPLICATION_PATH,'oslc-zend/application/');

require_once APPLICATION_PATH.'controllers/oslc.inc.php';

//defined('TRACKER_TYPE')
//    || define('TRACKER_TYPE', 'demo');

//require_once 'oslc-zend/application/models/ChangeRequests.php';

class OslcConnectorTest extends PHPUnit_Framework_TestCase
{
	protected $oslcconnector;
	
	protected function setUp() {
		$this->markTestSkipped('Set aside for now.');
		print_r('TRACKER_TYPE: '.TRACKER_TYPE);
		//define('TRACKER_TYPE', 'demo');
		$this->oslcconnector = new OslcConnector();
	}
	
	// test connector init without args
	public function testInitNoParams() {
		$params = $this->oslcconnector->init();

		// should return an empty array
		$this->assertTrue(is_array($params));
		$this->assertEquals(0, count($params));
	}
	
	// test connector init with incorrect param
	public function testInitParamsWrong() {
		$params = array('id'=>'coin');

		// should fail with exception
		try {
            $params = $this->oslcconnector->init($params);
        }
        catch (Exception $expected) {
        	$this->assertEquals("Unknown resource coin !",$expected->getMessage());
            return;
        }
        $this->fail('An expected exception has not been raised.');
	}
	
	// test connector init with all bugs requested
	public function testInitParamsAll() {
		$params = array('id'=>'bug');
		$params = $this->oslcconnector->init($params);

		// should be an empty array
		$this->assertTrue(is_array($params));
		$this->assertEquals(0, count($params));
	}
	
	// test connector init with all bugs requested variant
	public function testInitParamsAll2() {
		$params = array('id'=>'bugs');
		//print_r($params);
		$params = $this->oslcconnector->init($params);

		$this->assertTrue(is_array($params));
		$this->assertEquals(0, count($params));
	}
	
	// test connector init with one bug requested
	public function testInitParamsOne() {
		$params = array('bug'=>'1234');

		$params = $this->oslcconnector->init($params);
		
		$this->assertTrue(is_array($params));
		$this->assertEquals(1, count($params));
		$this->assertEquals("1234", $params['bug']);
	}
	
	// test connector init with one project requested
	public function testInitParamsProject() {
		$params = array('project'=>'12345');

		$params = $this->oslcconnector->init($params);
		
		$this->assertTrue(is_array($params));
		$this->assertEquals(1, count($params));
		$this->assertEquals("12345", $params['project']);
	}
	
	
}

?>