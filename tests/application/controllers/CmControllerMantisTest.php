<?php

/*******************************************************************************************
 * This test requires that the mantis project 'private static $project' below
 *  have the following:
 * 		- 	the four values 'Branch 1.1','Branch 1.3','Branch 2.1' and 'Branch 2.2'
 * 			be included in the project versions
 * 		-	there should be a custom field named 'version_number' in which the
 * 			values '1.1' and '1.3' are allowed to be set
 * 
 * phpunit --verbose --bootstrap=tests_config.inc.php application/controllers/CmControllerMantisTest
 * *****************************************************************************************
 */

# First load the current tests environment (also phpunit's bootstrap file)
require_once dirname( dirname( dirname( __FILE__ ))). DIRECTORY_SEPARATOR . 'tests_config.inc.php';

# Standard Zend Tests includes
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';
require_once 'Zend/Application.php';

// In the context of this test, auth will be basic
defined('AUTH_TYPE') ||
	define('AUTH_TYPE', 'basic');

	
/**
 * This test is a Mantis one (phpunit --list-groups)
 * @group Mantis
 * @backupGlobals disabled
 */
class CmControllerMantisTest extends Zend_Test_PHPUnit_ControllerTestCase {
	
	private static $post_data = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<dc:title xmlns:dc="http://purl.org/dc/terms/">random post test</dc:title>
								<dc:type xmlns:dc="http://purl.org/dc/terms/">General</dc:type>
								<dc:creator xmlns:dc="http://purl.org/dc/terms/">mdhar</dc:creator>
								<dc:description xmlns:dc="http://purl.org/dc/terms/">chking post</dc:description>
								<mantisbt:priority xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Low</mantisbt:priority>
								<mantisbt:severity xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Feature</mantisbt:severity>
								<mantisbt:version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 1.1</mantisbt:version>
								<mantisbt:target_version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 2.1</mantisbt:target_version>
								<mantisbt:version_number xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">1.1</mantisbt:version_number>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 1</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 2</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 3</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';
	
	private static $put_data = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<mantisbt:status xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Assigned</mantisbt:status>
								<dc:description xmlns:dc="http://purl.org/dc/terms/">updating post with put operation</dc:description>
								<mantisbt:priority xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">High</mantisbt:priority>
								<mantisbt:severity xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Major</mantisbt:severity>
								<mantisbt:version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 1.3</mantisbt:version>
								<mantisbt:target_version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 2.2</mantisbt:target_version>
								<mantisbt:version_number xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">1.3</mantisbt:version_number>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 4</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 5</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';
	private static $post_notes = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 6</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 7</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 8</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';

	private static $login = "mdhar";
	private static $password = "madhumita84";
	private static $project = "test";
	private static $url = "garbage";
	
	public function setUp()
	{   
		$tracker_type = getenv('TRACKER_TYPE');
		
		// only pass the test if testing mantis
		if ($tracker_type != 'mantis') {
			$this->markTestSkipped('Only available for TRACKER_TYPE == "mantis", not '.$tracker_type);
        }
        
		$this->bootstrap = new Zend_Application(APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini');
		parent::setUp();
		
	}

	public function tearDown() {
    	$this->resetRequest();
    	$this->resetResponse();
    	parent::tearDown();
  	}

	
	public function testCmGetAllBugs()
	{
		$acceptArray = Array('application/atom+xml', 'application/json');
		
		foreach($acceptArray as $accept)
		{
			$this->request->setHeader("Accept",$accept);
			$_SERVER['HTTP_ACCEPT'] = $accept;
			$str = self::$login.':'.self::$password;
			$str = base64_encode($str);
			
			$this->request->setHeader("Authorization", 'Basic '.$str);
			$this->request->setMethod('GET');
			//print_r('here');
			$this->dispatch('/cm/project/'.self::$project);//print_r('here');
			//print ($this->getResponse()->getBody());
			$this->assertController('mantiscm');
			$this->assertAction('readResourceCollection');
			$this->assertResponseCode(200);
		}
		
		//$this->markTestSkipped("This test requires mantis to run");
	}
	
	public function testCmPostBug()
	{
		$acceptArray = Array('application/xml');
		
		foreach($acceptArray as $accept)
		{
			$this->request->setHeader("Content-Type", $accept);
			$_SERVER['HTTP_ACCEPT'] = $accept;
			
			$str = self::$login.':'.self::$password;
			$str = base64_encode($str);
			
			$this->request->setHeader("Authorization", 'Basic '.$str);
			$this->request->setMethod('POST');
			
			//$this->request->setPost(array('username' => 'foobar','password' => 'foobar'));
			$this->request->setPost(array("xml" => self::$post_data));
	        //$HTTP_RAW_POST_DATA = self::$post_data;
			
	        $this->dispatch('/cm/project/'.self::$project);
	        print_r($this->getResponse()->getBody());
	        $this->assertController('mantiscm');
			$this->assertAction('post');
			$this->assertResponseCode(201);
		}	
		$url = $this->getResponse()->getBody();
		return $url;
		
	}
	
	/**
	 * @depends testCmPostBug
	 */
	public function testCmGetPostedBugFormats($url)
	{
		//$url = '/cm/bug/1';
		$acceptArray = Array(	'application/x-oslc-cm-change-request+xml',
								'application/xml',
								'text/xml',
							 	'application/json',
							 	'application/x-oslc-cm-change-request+json');
		
		foreach($acceptArray as $accept)
		{
			$this->request->setHeader("Accept", $accept);
			$_SERVER['HTTP_ACCEPT'] = $accept;
			$str = self::$login.':'.self::$password;
			$str = base64_encode($str);
			
			$this->request->setHeader("Authorization", 'Basic '.$str);
			$this->request->setMethod('GET');
			//print_r($url);
			$this->dispatch($url);
			//print ($this->getResponse()->getBody());
			$this->assertController('mantiscm');
			$this->assertAction('readResource');
			$this->assertResponseCode(200);
		}
	}
	
	/**
	 * @depends testCmPostBug
	 */
	public function testCmGetPostedBug($url)
	{
		$accept = "application/xml";
		$this->request->setHeader("Accept", $accept);
		$_SERVER['HTTP_ACCEPT'] = $accept;
		$str = self::$login.':'.self::$password;
		$str = base64_encode($str);
		
		$this->request->setHeader("Authorization", 'Basic '.$str);
		$this->request->setMethod('GET');
		//print_r($url);
		$this->dispatch($url);
		//print ($this->getResponse()->getBody());
		$data = $this->getResponse()->getBody();
		$resource1 = $this->parseXML(trim($data));
		//print_r($resource1);
		$resource2 = $this->parseXML(self::$post_data);
		//print_r($resource2);
		foreach($resource2 as $key => $value)
		{
			if(($key!='type')&&($key!='notes'))
			{
				//print_r($resource2[$key]."---".$resource1[$key]);
				$this->assertEquals(strcasecmp($resource2[$key], $resource1[$key]), 0);
			}
		}
		$this->assertController('mantiscm');
		$this->assertAction('readResource');
		$this->assertResponseCode(200);
		
	}

	/**
	 * @depends testCmPostBug
	 */
	public function testCmPutBug($url)
	{
		$acceptArray = Array('application/xml');
		
		foreach($acceptArray as $accept)
		{
			$this->request->setHeader("Content-Type", $accept);
			$_SERVER['HTTP_ACCEPT'] = $accept;
			
			$str = self::$login.':'.self::$password;
			$str = base64_encode($str);
			
			$this->request->setHeader("Authorization", 'Basic '.$str);
			$this->request->setMethod('PUT');
			
			//as there isnt any setPut we revert to using the setPost method
	        $this->request->setPost(array("xml" => self::$put_data));
	        //$HTTP_RAW_POST_DATA = self::$post_data;
			$put_url = $url.'?oslc_cm.properties=mantisbt:severity,mantisbt:priority,mantisbt:notes,mantisbt:status,mantisbt:version,mantisbt:target_version,mantisbt:version_number,dc:description';
	        $this->dispatch($put_url);
	        //print_r($this->getResponse()->getBody());
	        $this->assertController('mantiscm');
			$this->assertAction('put');
			$this->assertResponseCode(200);
		}	
		
		return $url;
		
	}
	
	/**
	 * @depends testCmPutBug
	 */
	public function testCmGetPuttedBug($url)
	{
		$accept = "application/xml";
		$this->request->setHeader("Accept", $accept);
		$_SERVER['HTTP_ACCEPT'] = $accept;
		$str = self::$login.':'.self::$password;
		$str = base64_encode($str);
		
		$this->request->setHeader("Authorization", 'Basic '.$str);
		$this->request->setMethod('GET');
		//print_r($url);
		$this->dispatch($url);
		//print ($this->getResponse()->getBody());
		$data = $this->getResponse()->getBody();
		$resource1 = $this->parseXML(trim($data));
		//print_r($resource1);
		$resource2 = $this->parseXML(self::$put_data);
		//print_r($resource2);
		foreach($resource2 as $key => $value)
		{
			if(($key!='type')&&($key!='notes'))
			{
				//print_r($resource2[$key]."---".$resource1[$key]);
				$this->assertEquals(strcasecmp($resource2[$key], $resource1[$key]), 0);
			}
		}
		$this->assertController('mantiscm');
		$this->assertAction('readResource');
		$this->assertResponseCode(200);
		
	}
	
	/**
	 * @depends testCmPutBug
	 */
	public function testCmPostNotes($url)
	{
		$acceptArray = Array('application/xml');
		
		foreach($acceptArray as $accept)
		{
			$this->request->setHeader("Content-Type", $accept);
			$_SERVER['HTTP_ACCEPT'] = $accept;
			
			$str = self::$login.':'.self::$password;
			$str = base64_encode($str);
			
			$this->request->setHeader("Authorization", 'Basic '.$str);
			$this->request->setMethod('POST');
			
			//$this->request->setPost(array('username' => 'foobar','password' => 'foobar'));
			$this->request->setPost(array("xml" => self::$post_notes));
	        //$HTTP_RAW_POST_DATA = self::$post_data;
			$notes_url = $url.'/notes';
	        $this->dispatch($notes_url);
	        //print_r($this->getResponse()->getBody());
	        $this->assertController('mantiscm');
			$this->assertAction('post');
			$this->assertResponseCode(201);
		}	
		return $notes_url;
	}
	
	/**
	 * doesn't work for the bug notes just created in the previous tests
	 * tests however work for bugnotes created before
	 * @depends testCmPostNotes
	 */
	public function testCmGetPostedNotes($notes_url)
	{
		//$notes_url = '/cm/bug/106/notes';
		$accept = "application/atom+xml";
		$this->request->setHeader("Accept", $accept);
		$_SERVER['HTTP_ACCEPT'] = $accept;
		$str = self::$login.':'.self::$password;
		$str = base64_encode($str);
		
		$this->request->setHeader("Authorization", 'Basic '.$str);
		$this->request->setMethod('GET');
		//print_r($notes_url);
		$this->dispatch($notes_url);
		//print_r($this->getResponse()->getBody());
		$data = $this->getResponse()->getBody();
		$feed = new SimpleXMLElement(trim($data));
		$x = 8;
		foreach($feed->entry as $entry)
		{
			//print_r($entry->title);
			$dc = $entry->content->children('http://purl.org/dc/terms/'); 
			//echo $dc->description;
			$this->assertEquals(strcmp($dc->description,'comment '.$x--), 0);
			
		}
		$this->assertController('mantiscm');
		$this->assertAction('readBugnoteCollection');
		$this->assertResponseCode(200);
		
	}
	
	private function parseXML($str)
	{
		$dc_attr = array("title", "identifier", "type", "description","subject","creator","modified","name","created");
		$mantisbt_attr = array("severity","status","priority","branch","version", "target_version","version_number","notes");
		$resource = array();
		$xml = simplexml_load_string($str);
		$namespace = $xml->getNamespaces(true);
		if (($namespace['rdf'] == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#') && ($xml->getName() == 'RDF')) 
		{		
			foreach ($xml->children('http://open-services.net/xmlns/cm/1.0/') as $changerequest)
			{
				if ($changerequest->getName() == 'ChangeRequest')
				{
					foreach ($changerequest->children('http://purl.org/dc/terms/') as $child) {
						$field = $child->getName();
						if (in_array($field,$dc_attr))
						{
							$value = (string)$child;
							$resource[$field] = $value;
						}
					}
					$x = 0;
					foreach ($changerequest->children($namespace['mantisbt']) as $child)
					{
						$field = $child->getName();
						if (in_array($field,$mantisbt_attr))
						{
							$value = (string)$child;
							if(strcasecmp($field,"notes")==0) //creating array for storing mantisbt notes
							{
								if($x==0)
								{
									$resource[$field] = array();
								}
								$resource[$field][$x++] = $value;
							}
							else
							{
								$resource[$field] = $value;	
							}
						}
					}
				}
			}
		}
		return $resource;
	}

}

//}