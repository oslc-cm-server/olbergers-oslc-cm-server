<?php

define('USER', 'admin');
define('PASS', 'IOPMLKJN');
define('BASE_URL', 'http://127.0.0.1/~mdhar');
define('MANTIS_OSLC', '/mantisbt/oslc-zend');
define('PROJECT','test');

/*******************************************************************************************
 * This test requires that the mantis project 'PROJECT' defined above have the following:
 * 		- 	the four values 'Branch 1.1','Branch 1.3','Branch 2.1' and 'Branch 2.2'
 * 			be included in the project versions
 * 		-	there should be a custom field named 'version_number' in which the
 * 			values '1.1' and '1.3' are allowed to be set
 * *****************************************************************************************
 */

class mantisTest extends PHPUnit_Framework_TestCase
{
	private static $bugCreated = FALSE;
	private static $bugCreatedURL = "";
	private static $post_data = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<dc:title xmlns:dc="http://purl.org/dc/terms/">random post test</dc:title>
								<dc:type xmlns:dc="http://purl.org/dc/terms/">General</dc:type>
								<dc:creator xmlns:dc="http://purl.org/dc/terms/">mdhar</dc:creator>
								<dc:description xmlns:dc="http://purl.org/dc/terms/">chking post</dc:description>
								<mantisbt:priority xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Low</mantisbt:priority>
								<mantisbt:severity xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Feature</mantisbt:severity>
								<mantisbt:version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 1.1</mantisbt:version>
								<mantisbt:target_version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 2.1</mantisbt:target_version>
								<mantisbt:version_number xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">1.1</mantisbt:version_number>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 1</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 2</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 3</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';
	private static $put_data = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<mantisbt:status xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Assigned</mantisbt:status>
								<dc:description xmlns:dc="http://purl.org/dc/terms/">updating post with put operation</dc:description>
								<mantisbt:priority xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">High</mantisbt:priority>
								<mantisbt:severity xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Major</mantisbt:severity>
								<mantisbt:version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 1.3</mantisbt:version>
								<mantisbt:target_version xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">Branch 2.2</mantisbt:target_version>
								<mantisbt:version_number xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">1.3</mantisbt:version_number>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 4</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 5</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';
	private static $post_notes = '<?xml version="1.0"?>
						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							<oslc_cm:ChangeRequest xmlns:oslc_cm="http://open-services.net/xmlns/cm/1.0/">
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 6</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 7</mantisbt:notes>
								<mantisbt:notes xmlns:mantisbt="http://helios-platform.org/ontologies/mantisbt/">comment 8</mantisbt:notes>
							</oslc_cm:ChangeRequest>
						</rdf:RDF>';
	
	public function setUp()
	{
		$tracker_type = getenv('TRACKER_TYPE');
		
		// only pass the test if testing mantis
		if ($tracker_type != 'mantis') {
            $this->markTestSkipped('Only available for TRACKER_TYPE == "demo".');
        }
		
		parent::setUp();
	}
	
	public function testProjectBugs()
	{
		$url = "/cm/project/".PROJECT;
		$accept = "application/atom+xml";
		
		$acc = "Accept: ".$accept;
		$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
		$arr = array($acc, $authz);
		
		$url = BASE_URL.MANTIS_OSLC.$url;
		
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,$url);
		curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
		
		curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
		
		$data = curl_exec($s);
		$response = curl_getinfo($s);
		curl_close($s);
		//print_r($response);
		$this->assertEquals($response['http_code'], 200);
	}
	
	public function testALL()
	{
		$url = "/cm/project/".PROJECT;
		
		$content = "application/xml";
		$cont = "Content-Type: ".$content;
		$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
		$arr = array($cont, $authz);
		$post_url = BASE_URL.MANTIS_OSLC.$url;
		
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,$post_url);
		curl_setopt($s, CURLOPT_VERBOSE, TRUE);
		curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($s, CURLOPT_POST, TRUE);
		curl_setopt($s, CURLOPT_POSTFIELDS, self::$post_data);
		curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
		
		$data = curl_exec($s);
		$response = curl_getinfo($s);
		//print_r($this->error_check($s));
		curl_close($s);
		print_r($data);
		//print_r('INFO: '.$response);
		$this->assertEquals($response['http_code'], 201);
		
		if($response['http_code']==201)
		{
			self::$bugCreated = TRUE;
			self::$bugCreatedURL = $data;
			print_r(self::$bugCreatedURL);
		}
		
	//testGetPostedBug()
	
		if(self::$bugCreated == TRUE)
		{
			$get_url = self::$bugCreatedURL;
			$accept = "application/xml";
			
			$acc = "Accept: ".$accept;
			$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
			$arr = array($acc, $authz);
			
			$s = curl_init();
			curl_setopt($s,CURLOPT_URL,$get_url);
			curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
			
			curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
			
			$data = curl_exec($s);
			$response = curl_getinfo($s);
			curl_close($s);
			//print_r($data);
			$this->assertEquals($response['http_code'], 200);
			
			$resource1 = $this->parseXML(trim($data));
			//print_r($resource1);
			$resource2 = $this->parseXML(self::$post_data);
			//print_r($resource2);
			foreach($resource2 as $key => $value)
			{
				if(($key!='type')&&($key!='notes'))
				{
					//print_r($resource2[$key]."---".$resource1[$key]);
					$this->assertEquals(strcasecmp($resource2[$key], $resource1[$key]), 0);
				}
			}
			
		}
		else{
			$this->markTestSkipped('self::bugCreated == FALSE');
		}
		
	//testPutBug()
	
		if(self::$bugCreated == TRUE)
		{
			
			$fh = fopen('php://memory','w+');
			fwrite($fh, self::$put_data);
			rewind($fh);
			$put_url = self::$bugCreatedURL.'?oslc_cm.properties=mantisbt:severity,mantisbt:priority,mantisbt:notes,mantisbt:status,mantisbt:version,mantisbt:target_version,mantisbt:version_number,dc:description';
			//print_r('#######'.$put_url.'###########');
			
			$content = "application/xml";
			$cont = "Content-Type: ".$content;
			$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
			$arr = array($cont, $authz);
			
			$s = curl_init();
			curl_setopt($s,CURLOPT_URL,$put_url);
			//curl_setopt($s, CURLOPT_VERBOSE, TRUE);
			curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($s, CURLOPT_PUT, TRUE);
			curl_setopt($s, CURLOPT_INFILE, $fh);
			curl_setopt($s, CURLOPT_INFILESIZE, strlen(self::$put_data));
			curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
			
			$data = curl_exec($s);
			$response = curl_getinfo($s);
			//print_r($this->error_check($s));
			curl_close($s);
			print_r($data);
			$this->assertEquals($response['http_code'], 200);
			
			fclose($fh);
		}
		else{
			$this->markTestSkipped('self::bugCreated == FALSE');
		}
		
	//testGetPuttedBug()
	
		if(self::$bugCreated == TRUE)
		{
			$get_url = self::$bugCreatedURL;
			$accept = "application/xml";
			
			$acc = "Accept: ".$accept;
			$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
			$arr = array($acc, $authz);
			
			$s = curl_init();
			curl_setopt($s,CURLOPT_URL,$get_url);
			curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
			
			curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
			
			$data = curl_exec($s);
			$response = curl_getinfo($s);
			curl_close($s);
			//print_r($data);
			$this->assertEquals($response['http_code'], 200);
			
			$resource1 = $this->parseXML(trim($data));
			//print_r($resource1);
			$resource2 = $this->parseXML(self::$put_data);
			//print_r($resource2);
			foreach($resource2 as $key => $value)
			{
				if(($key!='type')&&($key!='notes'))
				{
					//print_r($resource2[$key]."---".$resource1[$key]);
					$this->assertEquals(strcasecmp($resource2[$key], $resource1[$key]), 0);
				}
			}
			
		}
		else{
			$this->markTestSkipped('self::bugCreated == FALSE');
		}
	//testPostNotes()
	
		if(self::$bugCreated == TRUE)
		{
			$content = "application/xml";
			$cont = "Content-Type: ".$content;
			$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
			$arr = array($cont, $authz);
			$post_notes_url = self::$bugCreatedURL.'/notes';
			
			$s = curl_init();
			curl_setopt($s, CURLOPT_URL,$post_notes_url);
			//curl_setopt($s, CURLOPT_VERBOSE, TRUE);
			curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($s, CURLOPT_POST, TRUE);
			curl_setopt($s, CURLOPT_POSTFIELDS, self::$post_notes);
			curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
			
			$data = curl_exec($s);
			$response = curl_getinfo($s);
			//print_r($this->error_check($s));
			curl_close($s);
			//print_r($data);
			//print_r('INFO: '.$response);
			$this->assertEquals($response['http_code'], 201);
		}
		else{
			$this->markTestSkipped('self::bugCreated == FALSE');
		}
	//testGetPostedNotes()
	
		if(self::$bugCreated == TRUE)
		{
			$get_url = self::$bugCreatedURL.'/notes';
			$accept = "application/atom+xml";
			
			$acc = "Accept: ".$accept;
			$authz = $authorization = "Authorization: Basic ".base64_encode(USER.":".PASS);
			$arr = array($acc, $authz);
			
			$s = curl_init();
			curl_setopt($s,CURLOPT_URL,$get_url);
			curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
			
			curl_setopt($s, CURLOPT_HTTPHEADER, $arr);
			
			$data = curl_exec($s);
			$response = curl_getinfo($s);
			curl_close($s);
			//print_r($data);
			$this->assertEquals($response['http_code'], 200);
			
			$feed = new SimpleXMLElement(trim($data));
			$x = 8;
			foreach($feed->entry as $entry)
			{
				//print_r($entry->title);
				$dc = $entry->content->children('http://purl.org/dc/terms/'); 
				//echo $dc->description;
				$this->assertEquals(strcmp($dc->description,'comment '.$x--), 0);
				
			}
		}
		else{
			$this->markTestSkipped('self::bugCreated == FALSE');
		}		
	}
		
	private function error_check($ss)
	{
		if(curl_errno($ss))
		{
			$error_info = "Error => ".curl_errno($ss)." : ".curl_error($ss);
		}
		else
		{
			$returncode = (int)curl_getinfo($ss, CURLINFO_HTTP_CODE);
			$error_info = "Return code : ".$returncode;
		}
		return $error_info;
	}
	
	private function parseXML($str)
	{
		$dc_attr = array("title", "identifier", "type", "description","subject","creator","modified","name","created");
		$mantisbt_attr = array("severity","status","priority","branch","version", "target_version","version_number","notes");
		$resource = array();
		$xml = simplexml_load_string($str);
		$namespace = $xml->getNamespaces(true);
		if (($namespace['rdf'] == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#') && ($xml->getName() == 'RDF')) 
		{		
			foreach ($xml->children('http://open-services.net/xmlns/cm/1.0/') as $changerequest)
			{
				if ($changerequest->getName() == 'ChangeRequest')
				{
					foreach ($changerequest->children('http://purl.org/dc/terms/') as $child) {
						$field = $child->getName();
						if (in_array($field,$dc_attr))
						{
							$value = (string)$child;
							$resource[$field] = $value;
						}
					}
					$x = 0;
					foreach ($changerequest->children($namespace['mantisbt']) as $child)
					{
						$field = $child->getName();
						if (in_array($field,$mantisbt_attr))
						{
							$value = (string)$child;
							if(strcasecmp($field,"notes")==0) //creating array for storing mantisbt notes
							{
								if($x==0)
								{
									$resource[$field] = array();
								}
								$resource[$field][$x++] = $value;
							}
							else
							{
								$resource[$field] = $value;	
							}
						}
					}
				}
			}
		}
		return $resource;
	}
	
	
}