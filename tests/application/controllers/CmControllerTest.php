<?php

require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';  
require_once 'Zend/Application.php';  

//define(APPLICATION_PATH,'oslc-zend/application/');
// Define the backend tracker type : by defaut : mantis

//defined('TRACKER_TYPE')
//    || define('TRACKER_TYPE', (getenv('TRACKER_TYPE') ? getenv('TRACKER_TYPE') : 'demo'));



defined('AUTH_TYPE')
    || define('AUTH_TYPE', (getenv('AUTH_TYPE') ? getenv('AUTH_TYPE') : 'basic'));
    
class CmControllerTest extends Zend_Test_PHPUnit_ControllerTestCase {

	public function setUp()
	{
		$tracker_type = getenv('TRACKER_TYPE');
		
		// only pass the test if testing mantis
		if ($tracker_type != 'demo') {
            $this->markTestSkipped('Only available for TRACKER_TYPE == "demo".');
        }
		
		$this->bootstrap = new Zend_Application('testing',
            APPLICATION_PATH . '/configs/application.ini');
		parent::setUp();
	}

	public function testCmIndex() {
		$this->dispatch('/cm');
		//print_r($this->getResponse());
		$this->assertController('cm');
		$this->assertAction('index');
		$this->assertQueryCount('h1', 1);
	}

	/**
	 * @dataProvider providerCmGetAllBugs
	 */
	public function testCmGetAllBugsWithProvider($path,$mime,$code, $controller='cm') {
		$this->request->setHeader("Accept",$mime);
		$this->dispatch($path);
		$this->assertController($controller);
		if($controller == 'cm')	$this->assertAction('readResourceCollection');
		//print_r($this->getResponse());
		$this->assertResponseCode($code);
	}
	
	public function providerCmGetAllBugs () {
		return array(
			array('/cm/bug','application/atom+xml',200),
			array('/cm/bugs','application/atom+xml',200),
			array('/cm/project','application/atom+xml',500,'error'),
			array('/cm/project/1','application/atom+xml',200),
			array('/cm/bugs','whatever/unsupported',415)
			);
	}
	
	/**
	 * @dataProvider providerCmGetOneBug
	 */
	public function testCmGetOneBugWithProvider($path, $mime, $code, $controller='cm') {
		$this->request->setHeader("Accept",$mime);
		$this->dispatch($path);
		$this->assertController($controller);
		if($controller == 'cm')	$this->assertAction('get');
		$this->assertResponseCode($code);
	}
	
	public function providerCmGetOneBug() {
		$bug_id = '12345';
		return array(
			array('/cm/bug/'.$bug_id,'whatever/unsupported',415),
			array('/cm/bug/whatever','application/x-oslc-cm-change-request+xml',404,'error'),
			array('/cm/bug/'.$bug_id,'application/x-oslc-cm-change-request+xml',200)
			);
	}
	/*
    * application/xml
    * text/xml
    * application/json
    * application/x-oslc-cm-change-request+json
    * text/html
    * application/xhtml+xml 
	*/
	
	public function testCmGetBugsUnknownAcceptType() {
		$this->request->setHeader("Accept","whatever/unsupported");
		$this->dispatch('/cm/bugs');
		$this->assertController('cm');
		$this->assertAction('readResourceCollection');
		$this->assertResponseCode(415);
	}

	public function testCmGetOneBugBasicAuth() {
		$this->request->setHeader("Accept",'application/x-oslc-cm-change-request+xml');
		
		$login = 'theuser';
		$password = 'testpass';
		$str = $login.':'.$password;
		$str = base64_encode($str);
		
		$this->request->setHeader("Authorization", 'Basic '.$str);
		//$this->request->setHeader("Authorization", 'Basic dGhldXNlcjp0ZXN0cGFzcw==');
		$this->dispatch('/cm/bug/1234');
		$this->assertController('cm');
		$this->assertAction('get');
		$this->assertResponseCode(200);
	}
	
	public function testCmGetOneBugBasicAuthWrongPass() {
		$this->request->setHeader("Accept",'application/x-oslc-cm-change-request+xml');
		
		$login = 'theuser';
		$password = 'wrongpassword';
		$str = $login.':'.$password;
		$str = base64_encode($str);
		
		$this->request->setHeader("Authorization", 'Basic '.$str);
		//$this->request->setHeader("Authorization", 'Basic dGhldXNlcjp0ZXN0cGFzcw==');
		$this->dispatch('/cm/bug/1234');
		$this->assertController('error');
		//$this->assertAction('get');
		$this->assertResponseCode(500);
	}
	
}