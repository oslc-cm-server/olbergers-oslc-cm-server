<?php

require_once 'PHPUnit/Framework.php';

require_once 'CmControllerMantisTest.php';

class AllControllerTests extends PHPUnit_Framework_TestSuite
{
    public static function suite()
    {
        $suite = new AllControllerTests('Controller Part');

        $suite->addTestSuite('CmControllerMantisTest');
        
        return $suite;
    }
}