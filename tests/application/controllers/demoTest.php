<?php

require_once 'oslcTest.php';

//print_r('initial TRACKER_TYPE: '.TRACKER_TYPE);

// Tests the OSCL-CM Demo server based on CSV file

class OslcCsvDemoConnectorTest extends OslcConnectorTest
{
	protected $csvfilename = 'tests/application/controllers/oslctest.csv';
	
	protected function setUp() {
		$tracker_type = getenv('TRACKER_TYPE');
		
		// only pass the test if testing mantis
		if ($tracker_type != 'demo') {
            $this->markTestSkipped('Only available for TRACKER_TYPE == "demo".');
        }
		$this->oslcconnector = new OslcCsvDemoConnector(array('csvfilename'=>$this->csvfilename));
	}
	
	protected function tearDown() {
		unset($this->oslcconnector);
	}
	
	// test loading from CSV file containing 2 bugs
	public function testGetResourceCollection() {
		$params = array('id'=>'bugs');

		$params = $this->oslcconnector->init($params);
		$return = $this->oslcconnector->getResourceCollection();

		$this->assertEquals(3, count($return));
	}
	
	
	public function testGetResourceCollectionProject() {
		//print_r('>>> test testGetResourceCollectionProject');
		$params = array('project'=>'1');
		
		$params = $this->oslcconnector->init($params);
		$return = $this->oslcconnector->getResourceCollection();
		
		$this->assertEquals(2, count($return));
		//print_r($return);
	}
	
	// TODO : createChangeRequest() test
	public function testcreateChangeRequest() {
		$cr = new ChangeRequest();
		$return = $this->oslcconnector->createChangeRequest($cr);
		$this->assertEquals(-1,$return);
	}
}

?>