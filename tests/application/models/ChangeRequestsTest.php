<?php
require_once 'PHPUnit/Framework.php';

getenv('TRACKER_TYPE')
    		|| putenv('TRACKER_TYPE=demo');
    		
require_once APPLICATION_PATH.'/models/ChangeRequests.php';


class ChangeRequestsCsvTest extends PHPUnit_Framework_TestCase
{
	protected $csvfilename = 'tests/application/controllers/oslctest.csv';
	
	protected $changerequests = null;
	
	protected function setUp() {
		$tracker_type = getenv('TRACKER_TYPE');
		
		// only pass the test if testing mantis
		if ($tracker_type != 'demo') {
            $this->markTestSkipped('Only available for TRACKER_TYPE == "demo".');
        }
        
		$this->changerequests = new ChangeRequestsCsv('tests/application/models/oslctest.csv');
	}
	
	// test loading from CSV file containing 3 bugs
	public function testGetResourceCollection() {
		//$this->changerequests->loadAllChangeRequests();
				// construct a list of all entries of the feed
		foreach ($this->changerequests as $identifier => $changerequest) {

			//print_r($identifier);
			//print_r($changerequest);
				
			/*
			 
			$feedentry = $this->prepareChangeRequest($changerequest);

			$feedentry['title'] = 'changerequest '.$identifier.' : '.$feedentry['resource']['dc:title'];
			$feedentry['id']= $uri.$identifier;

			$returned[] = $feedentry;*/
		}
		
		$this->assertEquals(3, count($this->changerequests));
		
	}	
}
?>