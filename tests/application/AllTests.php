<?php

require_once dirname( dirname( __FILE__ )). DIRECTORY_SEPARATOR . 'tests_config.inc.php';

require_once 'PHPUnit/Framework.php';
/*
getenv('MANTIS_CONFIG')
    		|| putenv('MANTIS_CONFIG='. dirname( dirname( __FILE__ )). DIRECTORY_SEPARATOR .'mantis-config_inc.php');

//print get_include_path();

require_once(dirname( dirname( dirname( __FILE__ ))). DIRECTORY_SEPARATOR .'oslc-zend/config.inc.php');
*/
require_once 'controllers/AllControllerTests.php';

class AllTests
{

    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('OSLC Server Test');

        $suite->addTest(AllControllerTests::suite());
        //$suite->addTest(Soap_AllTests::suite());

        return $suite;
    }
}

AllTests::suite()->run();

?>